/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.bean;

import java.text.SimpleDateFormat;  
import java.time.LocalDate;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;  
import javax.faces.bean.ManagedBean;  
import javax.faces.context.FacesContext;  

/**
 *
 * @author Lukas
 */
public class CalendarBean {

    private LocalDate date;
    private LocalDate today;

    /**
     * Creates a new instance of CalendarBean
     */
    @PostConstruct
    public void init() {
        this.today = LocalDate.now();
    }
    
    public CalendarBean() {
    }

    /*public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }

    public void click() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("form:display");
        requestContext.execute("PF('dlg').show()");
    }*/

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getToday() {
        return today;
    }

    public void setToday(LocalDate today) {
        this.today = today;
    }
    
    
}
