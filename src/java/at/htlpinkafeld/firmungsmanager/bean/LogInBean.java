/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.bean;

import at.htlpinkafeld.firmungsmanager.pojo.Person;
import at.htlpinkafeld.firmungsmanager.service.LogInService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author patri
 */

@ManagedBean
@SessionScoped
public class LogInBean {

    @ManagedProperty(value="#{LogInService}")
    private LogInService service;
    
    private List<Person> persons;
    private String email;
    private String password;
    private Person loggedPerson;
    
    public LogInBean() {
        persons = new ArrayList<Person>();
    }
    
    @PostConstruct
    public void init(){
        persons = service.getPersons();
    }

    public void setService(LogInService service) {
        this.service = service;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Object logIn(){
        for(Person help:persons){
            if(help.getMail().equals(email) && help.getPassword().equals(password)){
                loggedPerson = help;
                if(loggedPerson.isIsAdmin())
                    return "successAdmin";
                return "success";
            }
        }
        return "failure";
    }

    public Person getLoggedPerson() {
        return loggedPerson;
    }

    public void setLoggedPerson(Person loggedPerson) {
        this.loggedPerson = loggedPerson;
    }
    
}
