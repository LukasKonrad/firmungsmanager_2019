/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.bean;

import at.htlpinkafeld.firmungsmanager.service.LogInService;
import at.htlpinkafeld.firmungsmanager.service.ModulService;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import at.htlpinkafeld.firmungsmanager.pojo.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

/**
 *
 * @author patri
 */

@ManagedBean
@SessionScoped
public class ModulBean {

    @ManagedProperty(value="#{ModulService}")
    private ModulService service;
    
    private List<Modul> module;
    private Modul current;

    private String title;
    private int maxPoints;
 
    private String eventName;
    private String eventDesc;
    private int maxParticipants;
   
    public ModulBean() {
    }
    
    @PostConstruct
    public void init(){
        module = service.getModule();
        current = module.get(0);
    }

    public List<Modul> getModule() {
        return module;
    }

    public void setModule(List<Modul> module) {
        this.module = module;
    }
    
    public ModulService getService() {
        return service;
    }

    public void setService(ModulService service) {
        this.service = service;
    }

    public Modul getCurrent() {
        return current;
    }

    public void setCurrent(Modul current) {
        this.current = current;
    }
    
       public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }
    
    public Object register(Event e, Person p){
        service.addParticipants(e,p);
        return null;
    }
    
    public Object edit(Event e){
        return null;
    }
    
    public Object delete(){
        service.getModule().remove(current);
        if(service.getModule().isEmpty())
            current = null;
        else
            current = service.getModule().get(0);
        return null;
    }
    
    public Object create(){
        Modul newModul = new Modul(title,new ArrayList<>(),maxPoints);
        service.getModule().add(newModul);
        current = newModul;
        return null;
    }
    
    public Object deleteEvent(Event e){
        current.getEvents().remove(e);
        return null;
    }
    
    public Object createEvent(){
        Event e = new Event();
        e.setTitle(eventName);
        e.setDescription(eventDesc);
        e.setMaxParticipants(maxParticipants);
        current.getEvents().add(e);
        return null;
    }
}
