/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Lukas
 */
@ManagedBean(name="upload")
@RequestScoped
public class FileUploadBean {
    private UploadedFile file;
    private Map<String, UploadedFile> uploadedFiles;
    /**
     * Creates a new instance of FileUploadBean
     */
    public FileUploadBean() {
        this.uploadedFiles = new HashMap<>();
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public Map<String, UploadedFile> getUploadedFiles() {
        return uploadedFiles;
    }

    public void setUploadedFiles(Map<String, UploadedFile> uploadedFiles) {
        this.uploadedFiles = uploadedFiles;
    }
    
    public void upload() {
        if(file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            this.uploadedFiles.put(file.getFileName(), file);
        }
    }
     
    public void handleFileUpload(FileUploadEvent event) {
        this.file = event.getFile();
        FacesMessage msg = new FacesMessage("Succesful", this.file.getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        this.uploadedFiles.put(file.getFileName(), file);
    }
    
    public UploadedFile getFileByName(String filename){
        return this.uploadedFiles.get(filename);
    }
    
    public void deleteFile(String fileName){
        this.uploadedFiles.remove(fileName);
    }
    
    
}
