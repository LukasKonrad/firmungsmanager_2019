/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.bean;

import java.io.InputStream;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Lukas
 */
@ManagedBean
@RequestScoped
public class FileDownloadBean {
    private StreamedContent file;
    /**
     * Creates a new instance of FileDownloadBean
     */
    public FileDownloadBean() {
        /*InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/backgrounds/phjg.jpg");
        file = new DefaultStreamedContent(stream, "image/jpg", "downloaded_the_picture");*/
        //availableFiles = FacesContext.getCurrentInstance().getExternalContext().getContext().getClass().getMethod("getUploadedFiles", null);
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("resources/backgrounds/phjg.jpg");
        file = new DefaultStreamedContent(stream);
        
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }
}
