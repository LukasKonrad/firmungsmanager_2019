/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.service;

import at.htlpinkafeld.firmungsmanager.pojo.*;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author patri
 */

@ManagedBean(name="ModulService", eager = true)
@SessionScoped
public class ModulService {
    
    ArrayList<Modul> module;
    
    public ModulService(){
        ArrayList events = new ArrayList<Event>();
        events.add(new Event("Suppenküche","Austeilen von Suppe für Bedürftige",LocalDate.of(2019, Month.JANUARY, 1), LocalDate.of(2019, Month.JANUARY, 2), new ArrayList<Person>(),5,new Address(8010,"Sparbersbachgasse",58,"Graz")));
        events.add(new Event("Exkurs: Altersheim","Wir gehen zum Altersheim in Graz",LocalDate.of(2019, Month.JANUARY, 1), LocalDate.of(2019, Month.JANUARY, 2), new ArrayList<Person>(),5,new Address(8010,"Sparbersbachgasse",58,"Graz")));
        events.add(new Event("Exkurs: Krankenhaus","Wir gehen zum Krankenhaus in Graz",LocalDate.of(2019, Month.JANUARY, 1), LocalDate.of(2019, Month.JANUARY, 2), new ArrayList<Person>(),5,new Address(8010,"Sparbersbachgasse",58,"Graz")));
        
        module = new ArrayList<Modul>();
        module.add(new Modul("TotalSozial",events,2));
        module.add(new Modul("PrayStation",new ArrayList<Event>(),2));
    }
    
    public ArrayList<Modul> getModule() {
        return module;
    }

    public void setModule(ArrayList<Modul> module) {
        this.module = module;
    }
    
    
    public void addEvent(Modul m, Event e){
        m.getEvents().add(e);
    }
    
    public void removeEvent(Modul m, Event e){
        m.getEvents().remove(e);
    }

    public void addParticipants(Event e, Person p) {
        e.addParticipants(p);
    }
}
