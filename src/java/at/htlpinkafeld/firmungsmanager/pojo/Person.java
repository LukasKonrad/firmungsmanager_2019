/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Lukas
 */
public class Person {
    private int personId;
    private String firstname;
    private String lastname;
    private Address home;
    private String mail;
    private String password;
    private LocalDate birthdate;
    private boolean isAdmin;

    public Person(int personId, String firstname, String lastname, Address home, String mail, String password, LocalDate birthdate, boolean isAdmin) {
        this.personId = personId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.home = home;
        this.mail = mail;
        this.password = password;
        this.birthdate = birthdate;
        this.isAdmin = isAdmin;
    }

    public Person(int personId, String firstname, String lastname, int zip, String street, int housenumber, String location, String mail, String password, LocalDate birthdate,boolean isAdmin){
        this(personId, firstname, lastname, new Address(zip, street, housenumber, location), mail, password, birthdate,isAdmin);
    }

    public int getPersonId() {
        return personId;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public Address getHome() {
        return home;
    }

    public void setHome(Address home) {
        this.home = home;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }
    
    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    
    @Override
    public String toString() {
        return "Person{" + "personId=" + personId + ", firstname=" + firstname + ", lastname=" + lastname + ", home=" + home + ", mail=" + mail + ", password=" + password + ", birthdate=" + birthdate + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + this.personId;
        hash = 61 * hash + Objects.hashCode(this.firstname);
        hash = 61 * hash + Objects.hashCode(this.lastname);
        hash = 61 * hash + Objects.hashCode(this.home);
        hash = 61 * hash + Objects.hashCode(this.mail);
        hash = 61 * hash + Objects.hashCode(this.password);
        hash = 61 * hash + Objects.hashCode(this.birthdate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (this.personId != other.personId) {
            return false;
        }
        if (!Objects.equals(this.firstname, other.firstname)) {
            return false;
        }
        if (!Objects.equals(this.lastname, other.lastname)) {
            return false;
        }
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.home, other.home)) {
            return false;
        }
        if (!Objects.equals(this.birthdate, other.birthdate)) {
            return false;
        }
        return true;
    }
    
    
}