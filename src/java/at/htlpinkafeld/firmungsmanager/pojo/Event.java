/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Lukas
 */
public class Event {
    private String title;
    private String description;
    private LocalDate begin;
    private LocalDate end;
    private List<Person> participants;
    private int maxParticipants;
    private Address place;

    public Event(String title, String description, LocalDate begin, LocalDate end, List<Person> participants, int maxParticipants, Address place) {
        this.title = title;
        this.description = description;
        this.begin = begin;
        this.end = end;
        this.participants = participants;
        this.maxParticipants = maxParticipants;
        this.place = place;
    }

    public Event() {
        this("","",LocalDate.now(),LocalDate.now(),new ArrayList<>(),1,new Address());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getBegin() {
        return begin;
    }

    public void setBegin(LocalDate begin) {
        this.begin = begin;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public List<Person> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Person> participants) {
        this.participants = participants;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public Address getPlace() {
        return place;
    }

    public void setPlace(Address place) {
        this.place = place;
    }
    
    public boolean isOpen(){
        if(this.maxParticipants >= this.participants.size() || this.begin.isAfter(LocalDate.now()))
            return false;
        return true;
    }
    
    public boolean addParticipants(Person p){
        if(this.participants.size()<this.maxParticipants){
            this.participants.add(p);
            return true;
        }
        else
            return false;
    }

    @Override
    public String toString() {
        return "Event{" + "title=" + title + ", description=" + description + ", begin=" + begin + ", end=" + end + ", participants=" + participants + ", maxParticipants=" + maxParticipants + ", place=" + place + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.title);
        hash = 67 * hash + Objects.hashCode(this.description);
        hash = 67 * hash + Objects.hashCode(this.begin);
        hash = 67 * hash + Objects.hashCode(this.end);
        hash = 67 * hash + Objects.hashCode(this.participants);
        hash = 67 * hash + this.maxParticipants;
        hash = 67 * hash + Objects.hashCode(this.place);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Event other = (Event) obj;
        if (this.maxParticipants != other.maxParticipants) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.begin, other.begin)) {
            return false;
        }
        if (!Objects.equals(this.end, other.end)) {
            return false;
        }
        if (!Objects.equals(this.participants, other.participants)) {
            return false;
        }
        if (!Objects.equals(this.place, other.place)) {
            return false;
        }
        return true;
    }
    
    
}
