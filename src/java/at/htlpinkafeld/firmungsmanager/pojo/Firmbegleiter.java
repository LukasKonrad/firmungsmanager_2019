/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Lukas
 */
public class Firmbegleiter extends Person{
    private int fbId;
    private List<Gruppe> groups;
    private boolean isAdministrator;

    public Firmbegleiter(int fbId, List<Gruppe> groups, boolean isAdministrator, int personId, String firstname, String lastname, Address home, String mail, String password, LocalDate birthdate) {
        super(personId, firstname, lastname, home, mail, password, birthdate,true);
        this.fbId = fbId;
        this.groups = groups;
        this.isAdministrator = isAdministrator;
    }

    public Firmbegleiter(int fbId, List<Gruppe> groups, boolean isAdministrator, int personId, String firstname, String lastname, int zip, String street, int housenumber, String location, String mail, String password, LocalDate birthdate) {
        super(personId, firstname, lastname, new Address(zip, street, housenumber, location), mail, password, birthdate,true);
        this.fbId = fbId;
        this.groups = groups;
        this.isAdministrator = isAdministrator;
    }
    
    public int getFbId() {
        return fbId;
    }

    public void setFbId(int fbId) {
        this.fbId = fbId;
    }

    public List<Gruppe> getGroups() {
        return groups;
    }

    public void setGroups(List<Gruppe> groups) {
        this.groups = groups;
    }

    public boolean isIsAdministrator() {
        return isAdministrator;
    }

    public void setIsAdministrator(boolean isAdministrator) {
        this.isAdministrator = isAdministrator;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.fbId;
        hash = 97 * hash + Objects.hashCode(this.groups);
        hash = 97 * hash + (this.isAdministrator ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Firmbegleiter other = (Firmbegleiter) obj;
        if (this.fbId != other.fbId) {
            return false;
        }
        if (this.isAdministrator != other.isAdministrator) {
            return false;
        }
        if (!Objects.equals(this.groups, other.groups)) {
            return false;
        }
        return true;
    }
    
    
}
