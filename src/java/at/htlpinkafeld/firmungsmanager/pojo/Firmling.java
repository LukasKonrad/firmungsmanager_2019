/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Lukas
 */
public class Firmling extends Person{
    private int firmlingsId;
    private int score;
    private List<Modul> regModules;
    private boolean archived;
    private Gruppe group;

    public Firmling(int firmlingsId, int score, List<Modul> regModules, boolean archived, Gruppe group, int personId, String firstname, String lastname, Address home, String mail, String password, LocalDate birthdate) {
        super(personId, firstname, lastname, home, mail, password, birthdate, false);
        this.firmlingsId = firmlingsId;
        this.score = score;
        this.regModules = regModules;
        this.archived = archived;
        this.group = group;
    }

    public Firmling(int firmlingsId, int score, List<Modul> regModules, boolean archived, Gruppe group, int personId, String firstname, String lastname, int zip, String street, int housenumber, String location, String mail, String password, LocalDate birthdate) {
        super(personId, firstname, lastname, new Address(zip, street, housenumber, location), mail, password, birthdate, false);
        this.firmlingsId = firmlingsId;
        this.score = score;
        this.regModules = regModules;
        this.archived = archived;
        this.group = group;
    }
    
    public int getFirmlingsId() {
        return firmlingsId;
    }

    public void setFirmlingsId(int firmlingsId) {
        this.firmlingsId = firmlingsId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public List<Modul> getRegModules() {
        return regModules;
    }

    public void setRegModules(List<Modul> regModules) {
        this.regModules = regModules;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Gruppe getGroup() {
        return group;
    }

    public void setGroup(Gruppe group) {
        this.group = group;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.firmlingsId;
        hash = 67 * hash + this.score;
        hash = 67 * hash + Objects.hashCode(this.regModules);
        hash = 67 * hash + (this.archived ? 1 : 0);
        hash = 67 * hash + Objects.hashCode(this.group);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Firmling other = (Firmling) obj;
        if (this.firmlingsId != other.firmlingsId) {
            return false;
        }
        if (this.score != other.score) {
            return false;
        }
        if (this.archived != other.archived) {
            return false;
        }
        if (!Objects.equals(this.regModules, other.regModules)) {
            return false;
        }
        if (!Objects.equals(this.group, other.group)) {
            return false;
        }
        return true;
    }

        
}
