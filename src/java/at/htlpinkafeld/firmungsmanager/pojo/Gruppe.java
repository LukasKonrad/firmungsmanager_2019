/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.util.List;

/**
 *
 * @author Lukas
 */
public class Gruppe {
    private int gruppenID;
    private String name;
    private List<Firmling> firmlinge;
    private Firmbegleiter begleiter;
    private boolean isArchived;

    public Gruppe(int gruppenID, String name, boolean isArchived) {
        this.gruppenID = gruppenID;
        this.name = name;
        this.isArchived = isArchived;
    }

    public int getGruppenID() {
        return gruppenID;
    }

    public String getName() {
        return name;
    }

    public boolean isIsArchived() {
        return isArchived;
    }

    public void setGruppenID(int gruppenID) {
        this.gruppenID = gruppenID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIsArchived(boolean isArchived) {
        this.isArchived = isArchived;
    }

    public List<Firmling> getFirmlinge() {
        return firmlinge;
    }

    public void setFirmlinge(List<Firmling> firmlinge) {
        this.firmlinge = firmlinge;
    }

    public Firmbegleiter getBegleiter() {
        return begleiter;
    }

    public void setBegleiter(Firmbegleiter begleiter) {
        this.begleiter = begleiter;
    }
    
    
}
