/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.util.Objects;

public class Address {
    private int zip;
    private String street;
    private int housenumber;
    private String location;

    public Address(int zip, String street, int housenumber, String location) {
        this.zip = zip;
        this.street = street;
        this.housenumber = housenumber;
        this.location = location;
    }
    
    public Address(){
        this(0,"",0,"");
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(int housenumber) {
        this.housenumber = housenumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.zip;
        hash = 41 * hash + Objects.hashCode(this.street);
        hash = 41 * hash + this.housenumber;
        hash = 41 * hash + Objects.hashCode(this.location);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Address other = (Address) obj;
        if (this.zip != other.zip) {
            return false;
        }
        if (this.housenumber != other.housenumber) {
            return false;
        }
        if (!Objects.equals(this.street, other.street)) {
            return false;
        }
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return zip + " " + location + ", " + street + " " + housenumber;
    }
    
    
}
