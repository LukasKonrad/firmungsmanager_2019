/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.utility;

/**
 *
 * @author Lukas
 */
public class EventAdministration {
    // Refer to the Java quickstart on how to setup the environment:
    // https://developers.google.com/calendar/quickstart/java
    // Change the scope to CalendarScopes.CALENDAR and delete any stored
    // credentials.

    /**
     * ---Creating an event and setting its metadata---
     */
    /*Event event = new Event()
            .setSummary("Google I/O 2015")
            .setLocation("800 Howard St., San Francisco, CA 94103")
            .setDescription("A chance to hear more about Google's developer products.");

    DateTime startDateTime = new DateTime("2015-05-28T09:00:00-07:00");
    EventDateTime start = new EventDateTime()
            .setDateTime(startDateTime)
            .setTimeZone("America/Los_Angeles");

    event.setStart (start);

    DateTime endDateTime = new DateTime("2015-05-28T17:00:00-07:00");
    EventDateTime end = new EventDateTime()
            .setDateTime(endDateTime)
            .setTimeZone("America/Los_Angeles");

    event.setEnd (end);

    String[] recurrence = new String[]{"RRULE:FREQ=DAILY;COUNT=2"};

    event.setRecurrence (Arrays.asList
    
    (recurrence));

    EventAttendee[] attendees = new EventAttendee[]{
        new EventAttendee().setEmail("lpage@example.com"),
        new EventAttendee().setEmail("sbrin@example.com"),};

    event.setAttendees (Arrays.asList(attendees));

    EventReminder[] reminderOverrides = new EventReminder[]{
        new EventReminder().setMethod("email").setMinutes(24 * 60),
        new EventReminder().setMethod("popup").setMinutes(10),};
    Event.Reminders reminders = new Event.Reminders()
            .setUseDefault(false)
            .setOverrides(Arrays.asList(reminderOverrides));

    event.setReminders (reminders);

    String calendarId = "primary";
    event = service.events().insert(calendarId, event).execute();

    System.out.printf("Event created: %s\n", event.getHtmlLink());
     */
    /** --- update an existing event to add an attachement --- */
    
    /*public static void addAttachment(Calendar calendarService, Drive driveService, String calendarId,
            String eventId, String fileId) throws IOException {
        File file = driveService.files().get(fileId).execute();
        Event event = calendarService.events().get(calendarId, eventId).execute();

        List<EventAttachment> attachments = event.getAttachments();
        if (attachments == null) {
            attachments = new ArrayList<EventAttachment>();
        }
        attachments.add(new EventAttachment()
                .setFileUrl(file.getAlternateLink())
                .setMimeType(file.getMimeType())
                .setTitle(file.getTitle()));

        Event changes = new Event()
                .setAttachments(attachments);
        calendarService.events().patch(calendarId, eventId, changes)
                .setSupportsAttachments(true)
                .execute();
    }
    */
}
