/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.utility;

import at.htlpinkafeld.firmungsmanager.pojo.Modul;
import at.htlpinkafeld.firmungsmanager.service.ModulService;
import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author patri
 */

@FacesConverter("ModulConverter")
public class ModulConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Application app = fc.getApplication();
        ModulService m = app.evaluateExpressionGet(fc, "#{ModulService}", ModulService.class);
        for(Modul help:m.getModule()){
            if(help.getTitle().equals(string))
                return help;
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((Modul)o).getTitle();
    }
    
}
